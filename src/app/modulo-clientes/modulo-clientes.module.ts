import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SmartadminModule } from '../shared/smartadmin.module';

import { ModuloClientesRoutingModule } from './modulo-clientes.routing';

import { VenClienteService } from "./providers/ven-cliente.service";
import { ObservableService } from "../core/providers/observable.service";
import { AlertasService } from "../core/providers/alertas.service";

import { ListaClientesComponent } from './lista-clientes/lista-clientes.component';
import { FormularioClientesComponent } from './formulario-clientes/formulario-clientes.component';

@NgModule({
  imports: [
    CommonModule,
    SmartadminModule,
    ModuloClientesRoutingModule
  ],
  declarations: [ListaClientesComponent, FormularioClientesComponent],
  providers: [VenClienteService, ObservableService, AlertasService]
})
export class ModuloClientesModule { }
