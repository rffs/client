import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { Location } from '@angular/common';

import { VenCliente } from "../../library-models/library.models";

import { AlertasService } from "../../core/providers/alertas.service";
import { VenClienteService } from "../../modulo-clientes/providers/ven-cliente.service";

declare var $: any;

@Component({
  selector: 'app-lista-clientes',
  templateUrl: './lista-clientes.component.html',
  styleUrls: ['./lista-clientes.component.css']
})
export class ListaClientesComponent implements OnInit {

  private cliente: VenCliente;
  private clientes: VenCliente[];
  private cargandoDatos: boolean = true;

  constructor(private clienteService: VenClienteService, private alertasService: AlertasService) { }

  ngOnInit() {
    this.getClientes()
  }

  public atras() {
    window.history.back();
  }

  public deleteCliente(cliente: VenCliente) {
    $.SmartMessageBox({
      title: "<i class='fa fa-trash-o txt-color-orangeDark'></i> Eliminar cliente <span class='txt-color-orangeDark'><strong>" + $('#show-shortcut').text() + "</strong></span>",
      content: "¿ Desea eliminar el cliente ?",
      buttons: '[CANCELAR][ACEPTAR]'
    }, (ButtonPressed) => {
      if (ButtonPressed == "ACEPTAR") {
        this.cliente = Object.assign({}, cliente);
        this.eliminar();
      }
    });
  }

  private eliminar() {
    this.cliente.cliEstado = 'p';
    this.clienteService.saveCliente(this.cliente)
      .subscribe(response => {
        this.getClientes();
        this.cargandoDatos = true;
        this.alertasService.mostrarAlertaInfoGeneric();
      });

  }

  private getClientes() {
    this.clienteService.getClientes()
      .subscribe(response => {
        this.clientes = response;
        this.cargandoDatos = false;
      });
  }

}
