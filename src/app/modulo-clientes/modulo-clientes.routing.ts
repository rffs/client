import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListaClientesComponent } from './lista-clientes/lista-clientes.component';
import { FormularioClientesComponent } from './formulario-clientes/formulario-clientes.component';

const routes: Routes = [
  {
    path: '',
    component: ListaClientesComponent,
    data: {
      pageTitle: 'Clientes'
    }
  },
  {
    path: 'formulario',
    component: FormularioClientesComponent,
    data: {
      pageTitle: 'Crear cliente'
    }
  },
  {
    path: 'formulario/:id',
    component: FormularioClientesComponent,
    data: {
      pageTitle: 'Editar cliente'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ModuloClientesRoutingModule { }
