import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { Location } from '@angular/common';

import { VenClienteModel } from "./../models/ven-cliente.model";

import { AlertasService } from "../../core/providers/alertas.service";
import { VenClienteService } from "../../modulo-clientes/providers/ven-cliente.service";

declare var $: any;

@Component({
  selector: 'app-formulario-clientes',
  templateUrl: './formulario-clientes.component.html',
  styleUrls: ['./formulario-clientes.component.css']
})
export class FormularioClientesComponent implements OnInit {

  @ViewChild('form') form;
  private cliente: VenClienteModel;
  private dniCliente: string = "";
  private cargandoDatos: boolean;

  constructor(private clienteService: VenClienteService, private alertasService: AlertasService,
    private activatedRoute: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.initialValues();
    this.activatedRoute.params.subscribe(params => {
      if (params['id']) {
        this.dniCliente = params['id'];
        this.setFormulario(this.dniCliente);
      }
    });
  }

  public guardar(event) {
    if (event) {
      if (this.dniCliente == "") {
        this.createCliente(event);
      }
      else {
        this.editCliente(event)
      }
    }
    else {
      this.alertasService.mostrarAlertaWarning("Información del sistema",
        "Sus datos son incorrectos, por favor revise su información y vuelva a intentarlo");
    }
  }

  public atras() {
    window.history.back();
  }

  public resetForm(form) {
    $(form).trigger('reset');
    this.cliente.cliDescuento = 0;
    this.cliente.cliCreditoMonto = 0;
  }

  public getValidators() {
    return {
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: VenClienteModel.getValidators()
    };
  }

  public verificarCliente(dni: string) {
    if (this.dniCliente == "") {
      this.clienteService.getClienteByDni(dni)
        .subscribe(response => {
          if (response.length) {
            this.alertasService.mostrarAlertaWarning("Información del sistema", "El cliente ya está registrado");
            this.cliente.cliDni = "";
          }
        });
    }
  }

  private createCliente(event) {
    this.cliente.cliEstado = 'a';
    this.cliente.cliListaPrecio = "lista";
    this.clienteService.saveCliente(this.cliente)
      .subscribe(response => {
        this.alertasService.mostrarAlertaInfoGeneric();
        this.router.navigate(['./clientes']);

      });
  }

  private editCliente(cliente: VenClienteModel) {
    this.clienteService.saveCliente(this.cliente)
      .subscribe(response => {
        this.alertasService.mostrarAlertaInfoGeneric();
        this.router.navigate(['./clientes']);
      });
  }

  private initialValues() {
    this.cliente = new VenClienteModel();
    this.cliente.cliDniTipo = 'cedula'
    this.cliente.cliContabilidad = false;
    this.cliente.cliContribuyenteEspecial = false;
    this.cliente.cliDescuento = 0;
    this.cliente.cliCreditoMonto = 0;
  }

  private setFormulario(dniCliente: string) {
    this.cargandoDatos = true;
    this.clienteService.getClienteByDni(dniCliente).subscribe(response => {
      this.cliente = response[0];
      this.cargandoDatos = false;
    });
  }

}
