import { Injectable } from '@angular/core';

import { config } from "app/shared/smartadmin.config";
import { ObservableService } from "app/core/providers/observable.service";
import { VenClienteModel } from "./../models/ven-cliente.model";

@Injectable()
export class VenClienteService {

  constructor(private observableService: ObservableService) { }

  public getClientes() {
    return this.observableService.serviceGet(`${config.APIRest.url}/cliente`)
  }

  public saveCliente(cliente: VenClienteModel) {
    return this.observableService.servicePost(`${config.APIRest.url}/cliente`,
      cliente);
  }

  public getClienteByDni(dni: string) {
    return this.observableService.serviceGet(`${config.APIRest.url}/cliente/${dni}`);
  }

}
