import { TestBed, inject } from '@angular/core/testing';

import { VenClienteService } from './ven-cliente.service';

describe('VenClienteService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [VenClienteService]
    });
  });

  it('should ...', inject([VenClienteService], (service: VenClienteService) => {
    expect(service).toBeTruthy();
  }));
});
