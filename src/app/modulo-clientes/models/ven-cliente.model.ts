import { customValidators } from "app/core/validators/customValidators";
export class VenClienteModel {

  public cliDni: string;
  public cliCelular: string;
  public cliContabilidad: boolean;
  public cliContribuyenteEspecial: boolean;
  public cliCorreo: string;
  public cliCreditoMonto: number;
  public cliDescuento: number;
  public cliDireccion: string;
  public cliDniTipo: string;
  public cliEstado: string;
  public cliListaPrecio: string;
  public cliNombre: string;
  public cliTelefono: string;

  constructor() {

  }

  public static getValidators() {
    return {
      cliDni: {
        group: '.col-md-4',
        validators: {
          notEmpty: {
            message: 'El DNI es obligatorio.'
          },
          stringLength: {
            min: 10,
            max: 13,
            message: 'El DNI debe contener de 10 y 13 números.'
          },
          numeric: {
            message: 'El DNI solo permite números.'
          },
          callback: {
            message: 'El DNI ingresado es incorrecto.',
            callback: function (value, validator, $field) {
              return customValidators.validateDNIEcuador(value);
            }
          }
        }
      },

      cliNombre: {
        group: '.col-md-4',
        validators: {
          notEmpty: {
            message: 'El nombre es obligatorio.'
          },
          stringLength: {
            min: 6,
            max: 100,
            message: 'El nombre debe contener de 6 de 100 caracteres.',
          },
          regexp: {
            regexp: /^[a-zA-ZÑñáéíóúÁÉÍÓÚ ]*$/,
            message: 'El nombre solo permite letras.'
          }
        }
      },

      cliDireccion: {
        group: '.col-md-12',
        validators: {
          notEmpty: {
            message: 'La dirección es obligatorio.'
          },
          stringLength: {
            min: 10,
            max: 100,
            message: 'La dirección debe contener de 10 a 100 caracteres.',
          },
          regexp: {
            regexp: /^[0-9a-zA-ZÑñáéíóúÁÉÍÓÚ ]*$/,
            message: 'La dirección solo permite letras y números.'
          }
        }
      },

      cliCreditoMonto: {
        group: '.col-md-4',
        validators: {
          notEmpty: {
            message: 'El monto crédito es obligatorio.'
          },
          numeric: {
            message: 'El valor ingresado no es un número',
            decimalSeparator: '.'
          },
          greaterThan: {
            value: 0,
            message: 'El monto crédito debe ser mayor o igual a cero.'
          }
        }
      },

      cliDescuento: {
        group: '.col-md-4',
        validators: {
          notEmpty: {
            message: 'El descuento es obligatorio.'
          },
          numeric: {
            message: 'El valor ingresado no es un número',
            decimalSeparator: '.'
          },
          greaterThan: {
            value: 0,
            message: 'El descuento debe ser mayor o igual a cero.'
          }
        }
      },

      cliCorreo: {
        group: '.col-md-4',
        validators: {
          emailAddress: {
            message: 'Correo eletrónico no es válido'
          }
        }
      },

      cliCelular: {
        group: '.col-md-4',
        validators: {
          stringLength: {
            min: 10,
            max: 10,
            message: 'El celular debe contener 10 números'
          },
          numeric: {
            message: 'El valor ingresado no es un número'
          },
        }
      },

      cliTelefono: {
        group: '.col-md-4',
        validators: {
          stringLength: {
            min: 9,
            max: 9,
            message: 'El teléfono debe contener 9 números'
          },
          numeric: {
            message: 'El valor ingresado no es un número'
          },
        }
      },

    }
  }

}
