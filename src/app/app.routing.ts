import { Routes, RouterModule } from '@angular/router';
import { MainLayoutComponent } from "./shared/layout/app-layouts/main-layout.component";
import { AuthLayoutComponent } from "./shared/layout/app-layouts/auth-layout.component";
import { ModuleWithProviders } from "@angular/core";

export const routes: Routes = [
  {
    path: '',
    component: MainLayoutComponent,
    children: [
      {
        path: '', redirectTo: 'clientes', pathMatch: 'full',
      },
      {
        path: 'clientes', loadChildren: 'app/modulo-clientes/modulo-clientes.module#ModuloClientesModule', data: { pageTitle: 'Clientes' }
      },
    ]
  },

  { path: '**', redirectTo: '' }
  //
];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes, { useHash: true });
