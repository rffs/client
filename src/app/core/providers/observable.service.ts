import { Headers, Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { AlertasService } from './alertas.service'

@Injectable()
export class ObservableService {

  private headers = new Headers({ 'Content-Type': 'application/json' });

  constructor(private http: Http, private alerta: AlertasService) { }

  public serviceGet(url: string) {
    return this.http.get(url/*, { withCredentials: true }*/)
      .map(response => response.json())
      .catch(err => this.alerta.mostrarAlertaErrorObservable(err));
  }

  public servicePost(url: string, objeto: any) {
    return this.http.post(url,
      JSON.stringify(objeto), { headers: this.headers/*, withCredentials: true*/ })
      .map(response => response.json())
      .catch(err => this.alerta.mostrarAlertaErrorObservable(err));
  }

  public servicePut(url: string, objeto: any) {
    return this.http.put(url,
      JSON.stringify(objeto), { headers: this.headers/*, withCredentials: true*/ })
      .map(response => response.json())
      .catch(err => this.alerta.mostrarAlertaErrorObservable(err));
  }

  public serviceDelete(url: string, objeto?: any) {
    return this.http.put(url,
      JSON.stringify(objeto), { headers: this.headers/*, withCredentials: true*/ })
      .map(response => response.json())
      .catch(err => this.alerta.mostrarAlertaErrorObservable(err));
  }

  public serviceRemove(url: string) {
    return this.http.delete(url/*, { withCredentials: true }*/)
      .map(resul => null)
      .catch(err => this.alerta.mostrarAlertaErrorObservable(err));
  }
}
